CP="./config/:./target/classes/:./target/dependency/*"

MEMORY="-Xmx30g"
OPTIONS="$MEMORY -Xss40m -ea -cp $CP"
PACKAGE_PREFIX="edu.illinois.cs.cogcomp"

MAIN="$PACKAGE_PREFIX.wikiparse.jwpl.DataMachine"
MAIN="$PACKAGE_PREFIX.wikiparse.jwpl.ParsePages"
MAIN="$PACKAGE_PREFIX.wikiparse.jwpl.PageParser"
MAIN="$PACKAGE_PREFIX.wikiparse.jwpl.GermanSplitter"
MAIN="$PACKAGE_PREFIX.wikiparse.jwpl.OpenNLPSplitter"

time nice java $OPTIONS $MAIN $CONFIG_STR $*
