package edu.illinois.cs.cogcomp.wikiparse.jwpl;

import edu.illinois.cs.cogcomp.core.io.LineIO;
import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CoreMap;
import edu.stanford.nlp.util.PropertiesUtils;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

/**
 * Created by Shyam on 11/30/16.
 */

public class GermanSplitter {

    static int doc=0;
    public static String process(BufferedReader br, StanfordCoreNLP corenlp) throws IOException {
        try {
            String buffer="";
            String line;
            while ((line = br.readLine()) != null) {

                if(line.startsWith("#-EOD-#"))
                {
                    doc++;
                    printDoc(corenlp,buffer);
                    buffer="";
                }
                else
                    buffer+=line+"\n";

            }
            printDoc(corenlp,buffer);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            br.close();
        }
        return null;
    }

    private static void printDoc(StanfordCoreNLP corenlp, String text) {
//        System.out.println(text);
        Annotation document = new Annotation(text);
        corenlp.annotate(document);

        List<CoreMap> sentences = document.get(CoreAnnotations.SentencesAnnotation.class);

        for(CoreMap sentence: sentences) {
            List<CoreLabel> tokens = sentence.get(CoreAnnotations.TokensAnnotation.class);
//            System.out.println(tokens.size());
            if(tokens.size()<5) continue;
            for (CoreLabel token: tokens) {
                // this is the text of the token
                String word = token.get(CoreAnnotations.TextAnnotation.class);
                System.out.print(word + " ");
            }
            System.out.println();
        }
//        System.out.println(sentences.size());
    }

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(args[0]));
//        BufferedReader br = new BufferedReader(new FileReader("test_dewiki.txt"));
        Properties props = new Properties();
        props.load(new FileReader("german.properties"));
        StanfordCoreNLP corenlp = new StanfordCoreNLP(props);
        process(br,corenlp);
        // System.out.println(doc);

    }
}
