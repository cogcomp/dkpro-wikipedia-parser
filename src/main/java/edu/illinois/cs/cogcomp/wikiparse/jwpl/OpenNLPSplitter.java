package edu.illinois.cs.cogcomp.wikiparse.jwpl;

import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CoreMap;
import opennlp.tools.sentdetect.SentenceDetectorME;
import opennlp.tools.sentdetect.SentenceModel;
import opennlp.tools.tokenize.TokenizerME;
import opennlp.tools.tokenize.TokenizerModel;
import opennlp.tools.util.InvalidFormatException;
import org.apache.tools.ant.util.Tokenizer;

import java.io.*;
import java.util.List;

/**
 * Created by Shyam on 11/30/16.
 */
public class OpenNLPSplitter {

    static int doc=0;
    public static String process(BufferedReader br, SentenceDetectorME sdetector, TokenizerME tokenizer) throws IOException {
        try {
            String buffer="";
            String line;
            while ((line = br.readLine()) != null) {

                if(line.startsWith("#-EOD-#"))
                {
                    doc++;
                    printDoc(sdetector,tokenizer,buffer);
                    buffer="";
                }
                else
                    buffer+=line+"\n";

            }
            printDoc(sdetector,tokenizer,buffer);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            br.close();
        }
        return null;
    }

    private static void printDoc(SentenceDetectorME sdetector, TokenizerME tokenizer, String text) {
//        System.out.println(text);
        String sentences[] = sdetector.sentDetect(text);

        for(String sentence: sentences) {
            String tokens[] = tokenizer.tokenize(sentence);
            if(tokens.length<5) {
                continue;
            }
            for (String token: tokens) {
                // this is the text of the token
                String word = token;
                System.out.print(word + " ");
            }
            System.out.println();
        }
//        System.out.println(sentences.size());
    }

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(args[0]));
//        BufferedReader br = new BufferedReader(new FileReader("test_dewiki.txt"));
        // always start with a model, a model is learned from training data
        InputStream is = new FileInputStream("de-sent.bin");
        SentenceModel smodel = new SentenceModel(is);
        SentenceDetectorME sdetector = new SentenceDetectorME(smodel);

        InputStream ist = new FileInputStream("de-token.bin");
        TokenizerModel tmodel = new TokenizerModel(ist);
        TokenizerME tokenizer = new TokenizerME(tmodel);

        process(br,sdetector,tokenizer);
        is.close();
        ist.close();
    }

}
