package edu.illinois.cs.cogcomp.wikiparse.jwpl;

import de.tudarmstadt.ukp.wikipedia.api.*;
import de.tudarmstadt.ukp.wikipedia.api.exception.WikiApiException;
import de.tudarmstadt.ukp.wikipedia.api.exception.WikiInitializationException;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Created by Shyam on 11/29/16.
 */
public class PageParser {
    public static DatabaseConfiguration dbConfig = null;
    public static Wikipedia wiki = null;

    private ThreadPoolExecutor parsing=null;

    /**
     * Bounds the number concurrent executing thread to 1/2 of the cores
     * available to the JVM. If more jobs are submitted than the allowed
     * upperbound, the caller thread will be executing the job.
     * @return a fixed thread pool with bounded job numbers
     */
    public static ThreadPoolExecutor getBoundedThreadPool() {
        int coreCount = Runtime.getRuntime().availableProcessors();
        coreCount = Math.max(coreCount, 10);
        ThreadPoolExecutor executor = new ThreadPoolExecutor(
                coreCount / 2 + 1, // Core count
                coreCount, // Pool Max
                60, TimeUnit.SECONDS, // Thread keep alive time
                new ArrayBlockingQueue<Runnable>(coreCount),// Queue
                new ThreadPoolExecutor.CallerRunsPolicy()// Blocking mechanism
        );
        executor.allowCoreThreadTimeOut(true);
        return executor;
    }

    public PageParser(){
        parsing = getBoundedThreadPool();
        dbConfig = new DatabaseConfiguration();
        dbConfig.setHost("localhost");
        // Can also use "wiki" database
        dbConfig.setDatabase("dewiki");
        dbConfig.setUser("root");
        dbConfig.setPassword("pratima");
        dbConfig.setLanguage(WikiConstants.Language.german);
        try {
            wiki = new Wikipedia(dbConfig);
        } catch (WikiInitializationException e) {
            e.printStackTrace();
        }
    }

    public void parse()
    {
        PageIterator pages = new PageIterator(wiki, true, 50000); // 2 M pages in dewiki
        int num = 0;
        while (pages.hasNext()) {
            Page page = null;
            try {
                if ((page = pages.next()) != null) {
                    if (page.isRedirect() && page.isDisambiguation() && page.isDiscussion() &&
                            page.getTitle().getPlainTitle().startsWith("Liste der") &&
                            page.getTitle().getPlainTitle().startsWith("Liste von")) {
                    }
//                    String text = page.getPlainText(); // time consuming
//                    String text = page.getText(); // NOT time consuming
//                    System.out.println(page.getTitle()+" "+text.length());
//                System.out.println(text);
		    if (num < 825000)
			{
			    num++;
			    continue;
			}
                    parsing.execute(new PageParseWorker(page) {
                        @Override
                        public void processAnnotation(String text) {
                            if (text.length()< 100)
                                {
				    System.err.println("too short");
				    return;
				}
                            System.out.println(text+"\n"+"#-EOD-#");
                        }
                    });
                    num++;
                }
            }
            catch (WikiApiException e)
            {
                // e.printStackTrace();
                System.err.println("some error");
            }
            if (num % 5000 == 0)
            {
                System.err.print(num + " ... ");
//                if (num==30000)
//                    System.exit(0);
            }
        }
    }

    /**
     * Waits for all pasring jobs to finish If not called, there might be pages
     * still being parsed
     */
    public void finishUp() {
        parsing.shutdown();
        try {
            parsing.awaitTermination(1, TimeUnit.DAYS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    public static void main(String[] args) {
        PageParser parser = new PageParser();
        parser.parse();
        parser.finishUp();
    }
}
