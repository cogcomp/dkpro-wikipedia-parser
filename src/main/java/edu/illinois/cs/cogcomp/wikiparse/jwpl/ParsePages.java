package edu.illinois.cs.cogcomp.wikiparse.jwpl;

import de.tudarmstadt.ukp.wikipedia.api.*;
import de.tudarmstadt.ukp.wikipedia.api.exception.WikiApiException;
import de.tudarmstadt.ukp.wikipedia.api.exception.WikiInitializationException;
import de.tudarmstadt.ukp.wikipedia.api.exception.WikiTitleParsingException;

/**
 * Created by upadhya3 on 11/29/16.
 */
@Deprecated
public class ParsePages {
    public static DatabaseConfiguration dbConfig = null;
    public static Wikipedia wiki = null;

    static {
        dbConfig = new DatabaseConfiguration();
        dbConfig.setHost("localhost");
        // Can also use "wiki" database
        dbConfig.setDatabase("dewiki");
        dbConfig.setUser("root");
        dbConfig.setPassword("pratima");
        dbConfig.setLanguage(WikiConstants.Language.german);
        try {
            wiki = new Wikipedia(dbConfig);
        } catch (WikiInitializationException e) {
            e.printStackTrace();
        }
    }
    public static void main(String[] args) throws WikiApiException {
        PageIterator pages = new PageIterator(wiki, true, 50000); // 2 M pages in dewiki
        int num = 0;
        while (pages.hasNext()) {
            Page page = null;
            try {
                if ((page = pages.next()) != null) {
                    if (page.isRedirect() && page.isDisambiguation() && page.isDiscussion() &&
                            page.getTitle().getPlainTitle().startsWith("Liste der") &&
                            page.getTitle().getPlainTitle().startsWith("Liste von")) {
                    }
                    String text = page.getPlainText(); // time consuming
                   // String text = page.getText(); // NOT time consuming
                   // System.out.println(page.getTitle()+" "+text.length());
//                System.out.println(text);
                    num++;
                }
            }
            catch (WikiApiException e)
            {
                e.printStackTrace();
            }
            if (num % 5000 == 0)
            {
                System.out.print(num + " ... ");
//                if (num==30000)
//                    System.exit(0);
            }
        }
    }
}
